# MongoDB Quick Tip Mockup

A static mock-up for an inspection tool to integrate "byte-sized" learning of MongoDB into the development process by providing code-based suggestions.

*(This repository is complimentary to a suggestions document.)*

## Notes

  * This is merely a mockup to get a better understanding of the proposal.
  * UI is inspired by [MongoDB Developer Hub](https://developer.mongodb.com/).

## How to View the Static Mockup

  1. Download or clone the repo.
  2. Navigate to the repo's `src` folder.
  3. Drag `index.html` into a web browser window.
      * `index.html` *is dependent on all files located in* `src`

## Screenshot

![MongoDB quick tip mockup](screenshot-MongoDB-QuickTip-Mockup-LinneaJ.png)
